import React from "react";

import './Recipe.css';

import Ingredient from "../../ingredient/Ingredient";

const Recipe = ({recipe, onDelete}) => {
    return (
        <div className="recipe">
            <div className="infoCol">
                <h2>{recipe.title}</h2>
                <button className="deleteB" onClick={() => onDelete(recipe)}>DELETE</button>
                <p>Cook time: {recipe.cookTime} min</p>
                <textarea className="descriptionArea" readOnly cols="18" rows="5" value={recipe.description}></textarea>
            </div>
            <div className="ingCol">
                {recipe.ingredients.map((ingredient) => (
                    <Ingredient key={ingredient.id} ingredient={ingredient} />
                ))}
            </div>
        </div>
    )
}

export default Recipe;