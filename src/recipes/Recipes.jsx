import React from "react";

import './Recipes.css';

import Recipe from "./recipe/Recipe";

const Recipes = ({recipes, deleteRecipe}) => {
    return (
        <div className="recipeContainer">
            {recipes.map((recipe) => (
                <Recipe key={recipe.title} recipe={recipe} onDelete={deleteRecipe}/>
            ))}
        </div>
    )
}

export default Recipes;