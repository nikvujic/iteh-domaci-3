import React from "react";
import { useState } from 'react';

import './newRecipe.css';

import Ingredient from "../ingredient/Ingredient";
import { Recipe as RecipeModel } from "../models/Recipe"

const NewRecipe = ({allIngredients, addRecipe}) => {
    const [recipeTitle, setRecipeTitle] = useState('');
    const [cookTime, setCookTime] = useState('');
    const [description, setDescription] = useState('');
    const [ingredients, setIngredients] = useState([]);


    function onAdd() {
        if (validateInputs()) {
            addRecipe(new RecipeModel(recipeTitle, cookTime, description, ingredients));
            alert("New recipe added!");

            setRecipeTitle('');
            setCookTime('');
            setDescription('');
            setIngredients([]);
        }
    }

    const handleTitleChange = (e) => setRecipeTitle(e.target.value);
    const cookTimeChange = (e) => setCookTime(e.target.value);
    const handleDescriptionChange = (e) => setDescription(e.target.value);

    function validateInputs() {
        if (recipeTitle.length == 0) {
            alert("Title can't be empty");
            return false;
        }

        if (cookTime.length == 0) {
            alert("Cook time can't be empty");
            return false;
        }

        if (!isNaN(cookTime)) {
            try {
                let quantInt = parseInt(cookTime);
                if (quantInt < 0) {
                    alert("Cook time can't be negative");
                    return false;

                }
            } catch (e) {
                alert("Cook time is not a valid number");
                return false;
            }
        } else {
            alert("Cook time is not a number");
            return false;
        }

        if (description.length == 0) {
            alert("Description can't be empty");
            return false;
        }

        if (ingredients.length == 0) {
            alert("You must add at least one ingredient");
            return false;
        }

        return true;
    }

    const addIngredient = (ingredient) =>  {
        let exists = false;

        ingredients.map(ing => {
            if (ing.id == ingredient.id) {
                exists = true;
            }
        });
        
        if (!exists) {
            let quantity = prompt("Please add quantity:");

            if (quantity === null) {
                return;
            }

            if (!isNaN(quantity)) {
                try {
                    let quantInt = parseInt(quantity);
                    if (quantInt > 0) {
                        let newIng = Object.assign({}, ingredient);;
                        newIng.quantity = quantInt;
    
                        setIngredients([...ingredients, newIng]);
                    } else {
                        alert("Quantity can't be 0 or below");
                    }
                } catch (e) {
                    alert("Added value is not a valid number");
                }
            } else {
                alert("Added value is not a number");
            }
        }
 
    }

    return (
        <div className="container">
            <div className="inputColumn">
                <p className="inputName">Title</p>
                <input type="text" name="" id="" value={recipeTitle} onChange={handleTitleChange}/>

                <p className="inputName">Cook time</p>
                <input type="text" name="" id="" value={cookTime} onChange={cookTimeChange}/>

                <p className="inputName">Description</p>
                <textarea className="descriptionAreaInput" cols="18" rows="5" value={description} onChange={handleDescriptionChange}></textarea>

                <p className="inputName">Added ingredients:</p>
                <div className="addedIngCol">
                    {
                        (ingredients.length === 0) ? (<p>none</p>) : (
                            ingredients.map((ingredient) => (
                                <Ingredient key={ingredient.id} ingredient={ingredient} />
                            ))
                        )
                    }
                </div>


                <button className="addB" onClick={() => onAdd()}>ADD</button>
            </div>
            <div className="allIngCol">
                <div className="ingHDiv"><h3 className="ingH">Available ingredients:</h3></div>
                {allIngredients.map((ingredient) => (
                    <Ingredient key={ingredient.id} ingredient={ingredient} addIngredient={addIngredient}/>
                ))}
            </div>
        </div>
    )
}

export default NewRecipe;