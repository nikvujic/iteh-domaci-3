import React from "react";
import { ImPlus } from "react-icons/im"
import { Link } from "react-router-dom";

import './navBar.css';

function NavBar() {
    return (
        <div className="navBar">
            <Link to="/" className="title">Recipes</Link>
            <Link to="/addNew" className="addNew">
                <ImPlus />
            </Link>
        </div>
    );
}

export default NavBar;