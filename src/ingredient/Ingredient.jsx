import React from "react";

import './Ingredient.css';

const Ingredient = ({ingredient, addIngredient = () => {}}) => {
    return (
        <div className="ingredient" onClick={() => addIngredient(ingredient)}>
            <div className="nameCol">{ingredient.name}</div>
            <div className="quantityCol">{ingredient.quantity ? ingredient.quantity : ''}</div>
            <div className="unitCol">{ingredient.unit}</div>
        </div>
    )
}

export default Ingredient;