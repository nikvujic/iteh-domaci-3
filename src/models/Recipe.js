export class Recipe {
    constructor(title, cookTime, description, ingredients) {
        this.title = title;
        this.cookTime = cookTime;
        this.description = description;
        this.ingredients = ingredients;
    }
}