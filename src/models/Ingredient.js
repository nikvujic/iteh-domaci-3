export class Ingredient {
    constructor(id, name, unit, quantity) {
        this.id = id;
        this.name = name;
        this.unit = unit;
        this.quantity = quantity;
    }
}