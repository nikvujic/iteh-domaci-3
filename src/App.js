import './App.css';

import { useState } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

import { Recipe } from './models/Recipe';
import { Ingredient } from './models/Ingredient';

import NavBar from './navbar/navBar';
import Recipes from './recipes/Recipes';
import NewRecipe from './newRecipe/newRecipe';

function App() {

  const [recipes, setRecipes] = useState([
    new Recipe('Recept 1', 15, 'Opis recepta 1', [
      new Ingredient(13, "Sastojak 9", 'kom', 10),
      new Ingredient(14, "Sastojak 10", 'ml', 12),
    ]),
    new Recipe('Recept 2', 5, 'Opis recepta 2', [
      new Ingredient(1, "Sastojak 1", 'kg', 15),
      new Ingredient(2, "Sastojak 2", 'ml', 10),
      new Ingredient(3, "Sastojak 3", 'kg', 40),
      new Ingredient(4, "Sastojak 4", 'kg', 1),
      new Ingredient(5, "Sastojak 5", 'kom', 12),
      new Ingredient(6, "Sastojak 6", 'kom', 14),
      new Ingredient(7, "Sastojak 7", 'kom', 2),
      new Ingredient(8, "Sastojak 8", 'kom', 18),
    ]),
    new Recipe('Recept 3', 0, '///', [
      new Ingredient(20, "Sastojak 20", 'ml'),
      new Ingredient(21, "Sastojak 21", 'kom'),
      new Ingredient(22, "Sastojak 22", 'ml'),
      new Ingredient(23, "Sastojak 23", 'kom'),
      new Ingredient(24, "Sastojak 24", 'ml'),
    ]),
    new Recipe('Recept 4', 25, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', [
      new Ingredient(9, "Sastojak 11", 'kom', 10),
      new Ingredient(10, "Sastojak 12", 'ml', 12),
      new Ingredient(11, "Sastojak 13", 'kom', 10),
      new Ingredient(12, "Sastojak 14", 'ml', 12),
    ]),
  ]);

  const allIngredients = [
    new Ingredient(1, "Sastojak 1", 'kg'),
    new Ingredient(2, "Sastojak 2", 'ml'),
    new Ingredient(3, "Sastojak 3", 'kg'),
    new Ingredient(4, "Sastojak 4", 'kg'),
    new Ingredient(5, "Sastojak 5", 'kom'),
    new Ingredient(6, "Sastojak 6", 'kom'),
    new Ingredient(7, "Sastojak 7", 'kom'),
    new Ingredient(8, "Sastojak 8", 'kom'),
    new Ingredient(9, "Sastojak 9", 'kom'),
    new Ingredient(10, "Sastojak 12", 'ml'),
    new Ingredient(11, "Sastojak 13", 'kom'),
    new Ingredient(12, "Sastojak 14", 'ml'),
    new Ingredient(13, "Sastojak 13", 'kom'),
    new Ingredient(14, "Sastojak 14", 'ml'),
    new Ingredient(15, "Sastojak 15", 'kom'),
    new Ingredient(16, "Sastojak 16", 'ml'),
    new Ingredient(17, "Sastojak 17", 'kom'),
    new Ingredient(18, "Sastojak 18", 'ml'),
    new Ingredient(19, "Sastojak 19", 'kom'),
    new Ingredient(20, "Sastojak 20", 'ml'),
    new Ingredient(21, "Sastojak 21", 'kom'),
    new Ingredient(22, "Sastojak 22", 'ml'),
    new Ingredient(23, "Sastojak 23", 'kom'),
    new Ingredient(24, "Sastojak 24", 'ml'),
  ];

  const addRecipe = (recipe) => {
    setRecipes([...recipes, recipe]);
  }
  
  function deleteRecipe(recipeToDel) {
    if (recipeToDel == null || recipeToDel === undefined) {
      return;
    }

    const newRecipes = recipes.filter((recipe) => recipe !== recipeToDel);
    setRecipes(newRecipes);
  }

  return (
    <div className="app">
      <BrowserRouter>
        <NavBar />
        <Routes>
          <Route 
            path="/"
            element = {
              <Recipes recipes={recipes} deleteRecipe={deleteRecipe} />
            }
          />
          <Route 
            path='/addNew'
            element = {
              <NewRecipe allIngredients={allIngredients} addRecipe={addRecipe}/>
            }
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
