Potrebno je kreirati statički veb sajt korišćenjem ReactJS veb okvira.

Zahtevi zadatka su sledeći:

* Sajt treba da sadrži najmanje dve stranice (komponente) čiji se sadržaj razlikuje (npr. početna i kontakt stranica). Za njihov prikaz koristiti koncepte rutiranja
* Kreirati najmanje dve reusable komponente (poput navigacionog menija ili polja forme)
* Potrebno je da neke od komponenata prosleđuju atribute i funkcije preko propertija
* Koristiti useState hook i/ili useEffect hook ili neke druge po želji u smislenoj situaciji
* Kreirati najmanje dve funkcionalnosti na sajtu korišćenjem JavaScripta ili JSXa
* Stilizovati sajt korišćenjem CSS ili bilo kog drugog proizvoljnog načina za stilizovanje (dozvoljeno je korišćenje pretprocesora i drugih okvira za stilizovanje, kao što su ScSS, Bootstrap, Tailwind...)
* Kreirati prateću dokumentaciju u okviru koje će biti predstavljen veb sajt i objašnjene karakteristične funkcionalnosti.
* Verzije aplikacije pratiti pomoću alata za verzionisanje koda - git (github)

Na moodle je potrebno okačiti dokumentaciju koja sadrži i link do repozitorijuma.